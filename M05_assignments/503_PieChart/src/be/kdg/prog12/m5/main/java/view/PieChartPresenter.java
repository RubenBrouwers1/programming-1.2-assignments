package be.kdg.prog12.m5.main.java.view;

import be.kdg.prog12.m5.main.java.model.Survey;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.stage.Window;

import java.util.Map;

public class PieChartPresenter {
    private Survey model;
    private PieChartView view;
    public PieChartPresenter(Survey model, PieChartView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }
    private void addEventHandlers() {
    // Add event handlers (inner classes or
    // lambdas) to view controls.
    // In the event handlers: call model methods
    // and updateView().
        view.getTextField().setOnAction(e -> setValuesToPie());
    }

    private void updateView() {
    // fills the view with model data
        this.view.getComboBox().getItems().clear();
        this.view.getComboBox().getItems().addAll(model.getPieChartData().keySet());
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        for(Map.Entry<String, Integer> entry : model.getPieChartData().entrySet()){
            pieChartData.add(new PieChart.Data(entry.getKey(), entry.getValue()));
        }
        this.view.getPie().setData(pieChartData);
    }

    public void setValuesToPie(){
        try {
            //1107
            Integer value = Integer.valueOf(view.getTextField().getText().trim());
            String answer = view.getComboBox().getValue().toString();
            model.setEntry(answer, value);
            updateView();
        } catch(NumberFormatException e){
            view.getTextField().setText(" ");
        }

    }

    public void addWindowEventHandlers () {
        Window window = view.getScene().getWindow();
    }
}
