package be.kdg.prog12.m5.main.java;

import be.kdg.prog12.m5.main.java.model.Survey;
import be.kdg.prog12.m5.main.java.view.PieChartPresenter;
import be.kdg.prog12.m5.main.java.view.PieChartView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.stage.Stage;

public class main extends Application{
    @Override
    public void start(Stage primaryStage) {
        Survey model = new Survey();
        model.setEntry("Text", 1);
        model.setEntry("Code", 19);
        model.setEntry(":)", 80);
        PieChartView view = new PieChartView();
        PieChartPresenter presenter = new PieChartPresenter(model, view);
        primaryStage.setScene(new Scene(view));
        presenter.addWindowEventHandlers();
        primaryStage.show();
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
}
