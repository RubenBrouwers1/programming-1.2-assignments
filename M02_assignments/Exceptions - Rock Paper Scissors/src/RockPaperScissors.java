import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        System.out.println();
        Scanner sc = new Scanner(System.in);
        Random rd = new Random();

        String userInput ="";
        boolean userInputCor = false;
        final String[] options = {"rock", "paper", "scissors"};

        System.out.println("This is a game of \"Rock Paper Scissors\"");
        System.out.println("------");

        while (true){
            System.out.print("Please enter 'rock', 'paper', or 'scissors' (or 'stop'): ");
            do {
                    userInput = sc.nextLine();
                    if (userInput.equalsIgnoreCase("stop")) {
                        System.exit(0);
                    }else if (!(userInput.equalsIgnoreCase("rock") || userInput.equalsIgnoreCase("paper") ||
                            userInput.equalsIgnoreCase("scissors"))) {
                        throw new InputMismatchException("Input is invalid");
                    }
                    else if (userInput.equalsIgnoreCase("rock") || userInput.equalsIgnoreCase("paper") ||
                            userInput.equalsIgnoreCase("scissors")) {
                        userInputCor = true;
                    }
            }while (!userInputCor);

            int compInput = rd.nextInt(2);

            System.out.printf("Your choice: %s\n", userInput);
            System.out.printf("My choice : %s\n", options[compInput]);


            if (userInput.equalsIgnoreCase(options[compInput])) {
                System.out.println("\tIt's a tie\n");
            } else if ((userInput.equalsIgnoreCase("rock") && compInput == 2) || (userInput.equalsIgnoreCase("paper") && compInput == 0) || (userInput.equalsIgnoreCase("scissors") && compInput == 1)) {
                System.out.println("\tCongratulations, You won!\n");
            } else if ((userInput.equalsIgnoreCase("rock") && compInput == 1) || (userInput.equalsIgnoreCase("paper") && compInput == 2) || (userInput.equalsIgnoreCase("scissors") && compInput == 0)) {
                System.out.println("\tI won!\n");
            }
        }
    }
}
