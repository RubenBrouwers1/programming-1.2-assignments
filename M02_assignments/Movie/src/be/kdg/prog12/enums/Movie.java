package be.kdg.prog12.enums;

public class Movie {
    String title;
    int year;
    Format format;
    Audio audio;

    public Movie(String title, int year, Format format, Audio audio) {
        this.title = title;
        this.year = year;
        this.format = format;
        this.audio = audio;
    }

    @Override
    public String toString() {
        return String.format("%-20s%-5d%-10s%-10s",title, year, format.toString(), audio.toString());
    }
}
