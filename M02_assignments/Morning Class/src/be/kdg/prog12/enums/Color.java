package be.kdg.prog12.enums;

public enum Color {
        WHITE("#FFFFFF"), BLACK("#000000"), RED("#FF0000"), GREEN("#008000"),
        BLUE("#0000FF"), YELLOW("#FFFF00"), PURPLE("#800080");

        private final String hexCode;

        Color(String hexCode){
                this.hexCode = hexCode;

                //Totally not possible
                //Color lightgreen = new Color(#...)
        }

        public String getHexCode() {
                return hexCode;
        }

        @Override
        public String toString() {
                return name().toLowerCase();
        }
}
