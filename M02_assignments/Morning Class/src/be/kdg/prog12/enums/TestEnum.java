package be.kdg.prog12.enums;

public class TestEnum {
    /*
    public static final int BLACK = 1;
    public static final int WHITE = 2;
    public static final int RED = 3;
    public static final int GREEN = 4;
    public static final int BLUE = 5;
    public static final int ORANGE = 6;
    public static final int YELLOW = 7;
    */

        public static void main(String[] args) {
            Color color;

            color = Color.BLUE;
            color = Color.RED;

            System.out.println("BLUE: " + Color.BLUE.toString());
            System.out.println("PURPLE: " + Color.PURPLE.name());
            System.out.println("RED: " + Color.RED.ordinal());
            System.out.println("GREEN: " + Color.GREEN.ordinal());

            Color[] allColors = Color.values();

            System.out.println("All colors: ");

            for (Color allColor : allColors) {
                System.out.printf("COLOR: %d %s %s\n", allColor.ordinal(), allColor.toString(), allColor.getHexCode());
            }

            if (color instanceof Color){
                System.out.println("Yes, it is an instance");
            }

            Color blue = Color.BLUE;
            System.out.printf("Blue's hexcode: %s\n", blue.getHexCode());

            //Color someColor = Color.of("#FFFFFF");
            //System.out.println(someColor);

        }

    }
