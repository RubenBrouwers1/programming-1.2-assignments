package be.kdg.prog12.inner;

public class Movie {
    public class Actor{
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Actor(String name) {
            this.name = name;
        }
    }

    private String title;
    private Integer year;
    private Actor leadingRole;
    private Actor supportingRole;

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public Actor getLeadingRole() {
        return leadingRole;
    }

    public Actor getSupportingRole() {
        return supportingRole;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setLeadingRole(Actor leadingRole) {
        this.leadingRole = leadingRole;
    }

    public void setSupportingRole(Actor supportingRole) {
        this.supportingRole = supportingRole;
    }

    public Movie(String title, Integer year, String leadingRoleName, String supportingRoleName) {
        this.title = title;
        this.year = year;
        //TODO= Like wtf??
        this.leadingRole = leadingRoleName == null ? null : new Actor(leadingRoleName);
        this.supportingRole = supportingRoleName == null ? null : new Actor(supportingRoleName);
    }

        /*if (leadingRole.name == null){
            this.leadingRole = null;
        } else {
            this.leadingRole = leadingRole;
        }

        if (supportingRole.name == null){
            this.supportingRole = null;
        } else {
            this.supportingRole = supportingRole;
        }*/

    public Movie(String title, int year) {
        this(title, year, null, null);
    }

    public Movie(String title) {
        this(title, null, null, null);
    }

    @Override
    public String toString() {
        return String.format("%-7s%-40s;%s%7d;%7s%s,%s", "Title: ", title, " Year: ", year, " Cast: ", leadingRole, supportingRole);
    }
}

