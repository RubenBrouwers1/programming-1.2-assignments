package be.kdg.prog12.enums;

public enum DayOfWeek {
    Monday(true), Tuesday(true), Wednesday(true), Thursday(true), Friday(true),
    Saturday(false), Sunday(false);

    private final boolean weekDay;

    DayOfWeek(boolean weekDay){
        this.weekDay = weekDay;
    }

    public boolean isWeekDay() {
        return weekDay;
    }

    @Override
    public String toString() {
        return String.format("%s (day %d, weekday = %b)", name(), ordinal()+1, isWeekDay());
    }
}
