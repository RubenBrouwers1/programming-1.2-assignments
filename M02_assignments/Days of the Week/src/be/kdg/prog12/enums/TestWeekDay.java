package be.kdg.prog12.enums;

public class TestWeekDay {
    public static void main(String[] args) {
        DayOfWeek[] day = DayOfWeek.values();

        for (DayOfWeek days : day) {
            System.out.println(days);
        }
    }
}
