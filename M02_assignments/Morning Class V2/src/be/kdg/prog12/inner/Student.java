package be.kdg.prog12.inner;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private StudentIDCard currentStudentID;
    private final List<StudentIDCard> pasStudentID;

    public Student() {
        //this.currentStudentID = null;
        this.pasStudentID = new ArrayList<>();
    }

    public static void setCurrentStudentID(StudentIDCard currentStudentID) {
        currentStudentID = currentStudentID;
    }

    public class StudentIDCard{
        private final String StudentNumber;

        public StudentIDCard(String studentNumber) {
            this.StudentNumber = studentNumber;
        }

        public void doSomeLogic(){
            //TODO
        }
    }
}
