package be.kdg.prog12.inner;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TestInner {
    public static void main(String[] args) {
        OuterClass oc = new OuterClass();
        OuterClass.InnerClass ic = oc.new InnerClass();

        Student student =  new Student();
        Student.setCurrentStudentID(student.new StudentIDCard("sdfqsdqsd"));

        Car car = new Car();
        car.setColor(Car.Color.BLUE);

        Map<String, Student> studentMap = new HashMap<>();
        studentMap.put("Levente", new Student());
        studentMap.put("Vasilena", new Student());
        studentMap.put("Assam", new Student());
        studentMap.put("Vlad", new Student());

        //Set<Map.Entry<String, Student>> entries = Student.entrySet();
        for (Map.Entry<String, Student> entries : studentMap.entrySet()){
            System.out.println(entries.getKey() + " / " + entries.getValue());
        }

    }
}
