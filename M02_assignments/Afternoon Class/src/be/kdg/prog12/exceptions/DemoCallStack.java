package be.kdg.prog12.exceptions;

public class DemoCallStack {
    public static void main(String[] args) {
    try {
        Book lotr = new Book("TT" /*null*/, "A 20th century classic.", 19.90, "J.R.R. Tolkien", "The Two Towers");

        System.out.println("Book: " + lotr);
    } catch (NullPointerException | ECommerceException e ){
        System.err.println("Please provide a product code");
    }
        System.out.println("Ending the program");
    }
}
