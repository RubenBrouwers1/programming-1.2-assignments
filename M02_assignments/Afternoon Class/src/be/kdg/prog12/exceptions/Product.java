package be.kdg.prog12.exceptions;

public class Product {
    private String code;
    private String description;
    private double price;

    public Product(String code, String description, double price) throws ECommerceException {
        if (code.length() < 3){
            throw new ECommerceException("Code should be at least 3 characters: ", code);
        }
        this.code = code;
        this.description = description;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
