package be.kdg.prog12.enums;

import java.util.Random;

public class PlayingCard {
    public enum Rank {
        TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN,
        JACK, QUEEN, KING, ACE;

        private int value;

        @Override
        public String toString() {
            return name().toLowerCase();
        }

        Rank() {
            if (ordinal() == 12) {
                value = 1;
            } else if (ordinal() < 8) {
                value = 2;
            } else {
                value = 10;
            }
        }

        public int getValue() {
            return value;
        }
    }
    public enum Suit{
        HEARTS, SPADES, CLUBS, DIAMONDS;

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    Random rd = new Random();
    Rank rank;
    Suit suit;

    public PlayingCard(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public int getValue(){
        return rank.getValue();
    }

    public static PlayingCard generateRandomPlayingCard(){
        Random rd = new Random();
        return new PlayingCard(Rank.values()[rd.nextInt(13)], Suit.values()[rd.nextInt(4)]);
    }

    @Override
    public String toString() {
        return String.format("%s of %s", rank, suit);
    }
}
