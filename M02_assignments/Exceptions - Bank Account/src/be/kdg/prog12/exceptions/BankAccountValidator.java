package be.kdg.prog12.exceptions;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.remainderUnsigned;

public class BankAccountValidator {
    private static final int LENGTH = 12;

    public static void validateAccount(String account)
            throws BankAccountException {
        if(account.length()!=12){
            throw new BankAccountException("BankAccount must have 12 digits");
        }
        if(!account.matches("//d")){
            throw new BankAccountException("String must be numeric");
        }
        if(!isValidNumber(account)){
            throw new BankAccountException("Wrong account number");
        }
    }

    private static boolean isValidNumber(String account) {
        //TODO: This also probably doesn't...
        long largeInt = Long.parseLong(account.substring(0, 10))%97;
        int controlNumber = Integer.parseInt(account.substring(10, 12));
        if (largeInt == 0){
            return controlNumber==97;
        } else {
            return largeInt == controlNumber;
        }
    }
}

 /*public static void validateAccount(String account) throws BankAccountException {
        try {
            if (account.length() != LENGTH) {
                throw new BankAccountException();
            }
        } catch (BankAccountException e){
            System.out.println("BankAccount must have 12 digits");
        }

        try {
            //TODO: This part does not work
            if (!account.matches("/d")) {
                throw new BankAccountException();
            }
        } catch (BankAccountException e){
            System.out.println("String must be numeric");
        }

        try{
            if (!isValidNumber(account)){
                throw new BankAccountException();
            }
        } catch (BankAccountException e){
            System.out.println("Wrong account number");
        }

    }*/