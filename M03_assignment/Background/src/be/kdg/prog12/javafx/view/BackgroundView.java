package be.kdg.prog12.javafx.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public class BackgroundView extends BorderPane {
    // private Node attributes (controls)
    private Button button;

    public BackgroundView () {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        // button = new Button("...")
        // label = new Label("...")
        button = new Button("Repaint");
    }

    private void layoutNodes() {
        // add/set … methodes
        // Insets, padding, alignment, …
        button.setPrefWidth(80.0);
        button.setPrefHeight(20.0);



        this.setBottom(button);

        this.setAlignment(button, Pos.BOTTOM_RIGHT);
        this.setMargin(button, new Insets(10.0));

        //BorderPane.setBorder()
        this.setPadding(new Insets(10.0));
    }

    // package-private Getters
    // for controls used by Presenter
    public Button getButton() {
        return button;
    }
}

