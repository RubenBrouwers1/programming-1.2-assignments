package be.kdg.prog12.retire;

import be.kdg.prog12.retire.model.Retirement;
import be.kdg.prog12.retire.view.RetirementPresenter;
import be.kdg.prog12.retire.view.RetirementView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    public static void main(String[] args){launch(args);}

    @Override
    public void start(Stage primaryStage) {
        Retirement model = new Retirement();
        RetirementView view = new RetirementView();
        new RetirementPresenter(model, view);
        primaryStage.setScene(new Scene(view));
        primaryStage.setTitle("Retirement Calculator");
        primaryStage.setWidth(400);
        primaryStage.setHeight(200);
        primaryStage.show();
    }
}
