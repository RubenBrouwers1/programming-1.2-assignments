package be.kdg.prog12.retire.view;
import be.kdg.prog12.retire.model.Retirement;
import javafx.scene.control.TextField;

public class RetirementPresenter {
    private Retirement model;
    private RetirementView view;

    public RetirementPresenter(
            Retirement model, RetirementView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        this.view.getButton().setOnAction(event -> {
            model.setBirthYear(Integer.parseInt(view.getInputField().getText()));
            updateView();
        });
    }
    private void updateView() {
        //TODO: 67 shows in the output field on startup of the program.
        this.view.getOutputField().setText(Integer.toString(this.model.getRetirementYear()));
    }
}

