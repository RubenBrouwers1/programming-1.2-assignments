package be.kdg.prog12.tts.view;

import be.kdg.prog12.tts.model.ScreenReader;
import javafx.event.ActionEvent;


public class TTSPresenter {
    private final ScreenReader model;
    private final TTSView view;
    //private int count = 1;

    public TTSPresenter(ScreenReader model, TTSView view){
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers(){
        this.view.getReadButton().setOnAction((/*ActionEvent*/ event) -> {
            this.model.setText(this.view.getUserText().getText());
            this.model.readAloud();
            /*
            System.out.printf("Button has been pressed %d times \n", count);
            count++;
            if (count > 20){
                System.out.println("STOP PRESSING THE DAMN BUTTON!!!");
                System.exit(69);
            }
             */
        });
    }

    private void updateView(){

    }
}
