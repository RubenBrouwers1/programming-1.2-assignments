package be.kdg.prog12.tts;

import be.kdg.prog12.tts.model.ScreenReader;
import be.kdg.prog12.tts.view.TTSPresenter;
import be.kdg.prog12.tts.view.TTSView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TTSApplication extends Application {
    @Override
    public void start(Stage stage) {
        ScreenReader model = new ScreenReader();
        TTSView ttsView = new TTSView();
        TTSPresenter ttsPresenter = new TTSPresenter(model, ttsView);
        Scene scene = new Scene(ttsView);
        stage.setScene(scene);

        stage.setTitle("TTS");
        stage.setMinWidth(100.0);
        stage.setMinHeight(100.0);
        stage.show();
    }
}
