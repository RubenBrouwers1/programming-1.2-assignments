package be.kdg.dice;

import be.kdg.dice.model.Dice;
import be.kdg.dice.view.DiceView;
import be.kdg.dice.view.Presenter;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{
    @Override
    public void start(Stage primaryStage) throws Exception {
        Dice model = new Dice();
        DiceView view = new DiceView();
        Presenter presenter = new Presenter(model, view);
        Scene scene = new Scene(view);
        primaryStage.setScene(scene);

        primaryStage.setTitle("Dice");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
