package be.kdg.dice.view;

import be.kdg.dice.model.Dice;
import javafx.scene.image.Image;

public class Presenter {
    private Dice model;
    private DiceView view;

    public Presenter(Dice model, DiceView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        this.view.getButton().setOnAction(event -> {
            this.model.werp();
            updateView();
        });
    }

    private void updateView() {
        //TODO: HUEEEEEEE?!?!?
        this.view.getImage1(imageView.setImage(new Image("/images/die1.png")));
    }
}
