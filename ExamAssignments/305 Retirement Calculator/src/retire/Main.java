package retire;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import retire.model.Retirement;
import retire.view.RetirementPresenter;
import retire.view.RetirementView;


public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Retirement model = new Retirement();
        RetirementView view = new RetirementView();
        new RetirementPresenter(model, view);
        primaryStage.setScene(new Scene(view));
        primaryStage.setTitle("Retirement Calculator");
        primaryStage.setWidth(555);
        primaryStage.setHeight(110);
        primaryStage.show();
    }
}
