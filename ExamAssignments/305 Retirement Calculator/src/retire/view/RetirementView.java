package retire.view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class RetirementView extends BorderPane {
    // private Node attributes (controls)

    private TextField inputField;
    private TextField outputField;
    private Button button;
    private HBox hBox;

    public RetirementView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        inputField = new TextField("YYYY");
        outputField = new TextField("");
        outputField.setEditable(false);
        button = new Button("BirthYear > RetirementYear");
        hBox = new HBox();
    }

    private void layoutNodes() {
        this.setCenter(hBox);
        hBox.getChildren().addAll(inputField, button, outputField);

        HBox.setMargin(inputField, new Insets(10, 10, 10, 10));

        HBox.setMargin(button, new Insets(10, 10, 10, 10));

        HBox.setMargin(outputField, new Insets(10, 10, 10, 10));

        this.setPadding(new Insets(10, 10, 10, 10));
    }

    // package-private Getters
    // for controls used by Presenter
    public TextField getInputField() {
        return inputField;
    }

    public TextField getOutputField() {
        return outputField;
    }

    public void setOutputField(TextField outputField) {
        this.outputField = outputField;
    }

    public Button getButton() {
        return button;
    }
}