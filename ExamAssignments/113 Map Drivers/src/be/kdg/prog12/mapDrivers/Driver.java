package be.kdg.prog12.mapDrivers;

import java.util.TreeSet;

public class Driver implements Comparable<Driver> {
    private final String name;
    private final String nationality;

    TreeSet<Integer> yearsTheDriverWasChampion = new TreeSet<>();

    //Constructor
    public Driver(String nationality, String name) {
        this.name = name;
        this.nationality = nationality;
    }

    //Getter
    public String getName() {
        return name;
    }

    public TreeSet<Integer> getYearsTheDriverWasChampion() {
        return yearsTheDriverWasChampion;
    }

    //Setters
    public void setYearsTheDriverWasChampion(Integer yearsTheDriverWasChampion) {
        this.yearsTheDriverWasChampion.add(yearsTheDriverWasChampion);
    }

    @Override
    public String toString() {
        return String.format("%s (%s) %d times champion %s\n", name, nationality, yearsTheDriverWasChampion.size(), yearsTheDriverWasChampion.toString());
    }

    @Override
    public int compareTo(Driver other) {
        int differenceChampionYears = this.yearsTheDriverWasChampion.size() - other.yearsTheDriverWasChampion.size();
        return (differenceChampionYears != 0) ? differenceChampionYears : Integer.compare(other.yearsTheDriverWasChampion.last(), this.yearsTheDriverWasChampion.last());
    }
}
