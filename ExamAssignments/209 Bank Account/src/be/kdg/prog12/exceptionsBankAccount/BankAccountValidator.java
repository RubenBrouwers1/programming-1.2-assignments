package be.kdg.prog12.exceptionsBankAccount;

public class BankAccountValidator {
    private static final int LENGTH = 12;

    public static void validateAccount(String account)
            throws BankAccountException {
        if (account.length() != LENGTH) {
            throw new BankAccountException("BankAccount must have 12 digits");
        } else if (!account.matches("[0-9]+")/*I just did '\\d', but apparently that didn't work...*/) {
            throw new BankAccountException("String must be numeric");
        } else if (!isValidNumber(account)) {
            throw new BankAccountException("Wrong account number");
        }
    }

    private static boolean isValidNumber(String account) {
        long largeInt = Long.parseLong(account.substring(0, 10)) % 97;
        int controlNumber = Integer.parseInt(account.substring(10, 12));
        if (largeInt == 0) {
            return controlNumber == 97;
        } else {
            return largeInt == controlNumber;
        }
    }
}