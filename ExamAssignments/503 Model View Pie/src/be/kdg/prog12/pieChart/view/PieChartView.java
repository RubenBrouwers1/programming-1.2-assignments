package be.kdg.prog12.pieChart.view;

import javafx.geometry.Insets;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;


public class PieChartView extends BorderPane {
    private PieChart pie;
    private ComboBox<String> comboBox;
    private TextField textField;
    private Label label;

    public PieChartView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.pie = new PieChart();
        this.comboBox = new ComboBox<>();
        this.comboBox.setEditable(true);
        this.textField = new TextField();
        this.label = new Label();
    }

    private void layoutNodes() {
        this.pie.setTitle("Where do I type semicolons?");
        this.setCenter(this.pie);
        this.setBottom(addHBox());
    }

    private HBox addHBox() {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(10));
        label.setText("Change a percentage");
        hBox.setSpacing(10);
        hBox.getChildren().addAll(label, comboBox, textField);
        return hBox;
    }

    public PieChart getPie() {
        return pie;
    }

    public ComboBox getComboBox() {
        return comboBox;
    }

    public TextField getTextField() {
        return textField;
    }
}

