package be.kdg.prog12.message;

import be.kdg.prog12.message.view.MessagePresenter;
import be.kdg.prog12.message.view.MessageView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MessageApplication extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        final MessageView view = new MessageView();
        new MessagePresenter(view);
        primaryStage.setScene(new Scene(view));
        primaryStage.setTitle("Message");
        // primaryStage.setResizable(false); // bugged under Linux
        primaryStage.show();
    }
}
