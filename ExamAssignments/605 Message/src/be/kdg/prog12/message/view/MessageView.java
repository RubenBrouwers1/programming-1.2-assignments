package be.kdg.prog12.message.view;

import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class MessageView extends BorderPane {
    private static final Font FONT = new Font("SansSerif Bold", 64.0);

    private Canvas canvas;
    private MenuItem load;
    private MenuItem save;
    private Label messageLabel;
    private TextField messageField;
    private Label foregroundLabel;
    private ColorPicker foregroundPicker;
    private Label backgroundLabel;
    private ColorPicker backgroundPicker;

    public MessageView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        canvas = new Canvas(800.0, 140.0);
        load = new MenuItem("Load");
        save = new MenuItem("Save");
        messageLabel = new Label("Message:");
        messageField = new TextField();
        foregroundLabel = new Label("Foreground");
        foregroundPicker = new ColorPicker();
        foregroundPicker.setValue(Color.BLACK);
        backgroundLabel = new Label("Background");
        backgroundPicker = new ColorPicker();
    }

    private void layoutNodes() {
        final Menu fileMenu = new Menu("File");
        fileMenu.getItems().add(load);
        fileMenu.getItems().add(save);
        final MenuBar menuBar = new MenuBar(fileMenu);

        final GridPane gridPane = new GridPane();
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(34.0);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(33.0);
        col2.setHalignment(HPos.RIGHT);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(33.0);
        col3.setHalignment(HPos.RIGHT);
        gridPane.getColumnConstraints().addAll(col1, col2, col3);
        gridPane.setVgap(5.0);
        gridPane.setHgap(5.0);
        gridPane.setPadding(new Insets(5.0));
        gridPane.add(messageLabel, 0, 0);
        gridPane.add(messageField, 0, 1);
        gridPane.add(foregroundLabel, 1, 0);
        gridPane.add(foregroundPicker, 1, 1);
        gridPane.add(backgroundLabel, 2, 0);
        gridPane.add(backgroundPicker, 2, 1);

        setTop(menuBar);
        setCenter(canvas);
        setBottom(gridPane);
    }

    void showMessage(String message, Color foreground, Color background) {
        final GraphicsContext gc = canvas.getGraphicsContext2D();
        final double width = canvas.getWidth();
        final double height = canvas.getHeight();

        final Text text = new Text(message);
        text.setFont(FONT);
        final Bounds bounds = text.getLayoutBounds();

        gc.setFont(FONT);

        gc.setFill(background);
        gc.fillRect(0.0, 0.0, width, height);

        gc.setFill(foreground);
        gc.fillText(message, (width - bounds.getWidth()) / 2.0, 90.0);
    }

    TextField getMessageField() {
        return messageField;
    }

    ColorPicker getForegroundPicker() {
        return foregroundPicker;
    }

    ColorPicker getBackgroundPicker() {
        return backgroundPicker;
    }

    MenuItem getLoadMenuItem() {
        return load;
    }

    MenuItem getSaveMenuItem() {
        return save;
    }
}
