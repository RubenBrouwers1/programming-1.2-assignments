package be.kdg.prog12.message.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.*;
import java.util.Base64;

public class MessagePresenter {
    private final MessageView view;

    public MessagePresenter(MessageView view) {
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        final EventHandler<ActionEvent> eventHandler = event -> updateView();
        view.getForegroundPicker().setOnAction(eventHandler);
        view.getBackgroundPicker().setOnAction(eventHandler);

        view.getMessageField().textProperty().addListener((observable, oldValue, newValue) -> updateView());

        view.getLoadMenuItem().setOnAction(event -> {
            FileChooser Open = new FileChooser();
            File selectedFile = Open.showOpenDialog(view.getScene().getWindow());

            if (selectedFile != null) {
                FileInputStream fileInputStream;
                try {
                    fileInputStream = new FileInputStream(selectedFile);
                    try {
                        try (DataInputStream dataInputStream = new DataInputStream(fileInputStream)) {

                            Short fRed = dataInputStream.readShort();
                            Short fGreen = dataInputStream.readShort();
                            Short fBlue = dataInputStream.readShort();

                            Short bRed = dataInputStream.readShort();
                            Short bGreen = dataInputStream.readShort();
                            Short bBlue = dataInputStream.readShort();

                            byte[] buffer = new byte[50];

                            int numberOfBytes = dataInputStream.read(buffer);

                            String someEncodedString = new String(buffer, 0, numberOfBytes);
                            String someDecodedString = new String(Base64.getDecoder().decode(someEncodedString));

//                            System.out.println(someDecodedString);
//                            System.out.println(fRed + " " + fGreen + " " + fBlue);
//                            System.out.println(bRed + " " + bGreen + " " + bBlue);

                            this.view.getMessageField().setText(someDecodedString);
                            this.view.getForegroundPicker().setValue(Color.rgb(fRed, fGreen, fBlue));
                            this.view.getBackgroundPicker().setValue(Color.rgb(bRed, bGreen, bBlue));

                            updateView();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        view.getSaveMenuItem().setOnAction(event -> {
            FileChooser Save = new FileChooser();
            File selectedFile = Save.showSaveDialog(view.getScene().getWindow());

            if (selectedFile != null) {
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(selectedFile);
                    try (DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream)) {

                        String message = this.view.getMessageField().getText();
                        Short fRed = (short) (this.view.getForegroundPicker().getValue().getRed() * 255);
                        Short fGreen = (short) (this.view.getForegroundPicker().getValue().getGreen() * 255);
                        Short fBlue = (short) (this.view.getForegroundPicker().getValue().getBlue() * 255);
                        Short bRed = (short) (this.view.getBackgroundPicker().getValue().getRed() * 255);
                        Short bGreen = (short) (this.view.getBackgroundPicker().getValue().getGreen() * 255);
                        Short bBlue = (short) (this.view.getBackgroundPicker().getValue().getBlue() * 255);

                        dataOutputStream.writeShort(fRed);
                        dataOutputStream.writeShort(fGreen);
                        dataOutputStream.writeShort(fBlue);
                        dataOutputStream.writeShort(bRed);
                        dataOutputStream.writeShort(bGreen);
                        dataOutputStream.writeShort(bBlue);

//                        System.out.println(fRed + " " + fGreen + " " + fBlue);
//                        System.out.println(bRed + " " + bGreen + " " + bBlue);
//                        System.out.println(message);

                        dataOutputStream.write(Base64.getEncoder().encode(message.getBytes()));

                        updateView();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateView() {
        final String message = view.getMessageField().getText();
        final Color foreground = view.getForegroundPicker().getValue();
        final Color background = view.getBackgroundPicker().getValue();
        view.showMessage(message, foreground, background);
    }
}
