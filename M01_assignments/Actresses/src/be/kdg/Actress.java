package be.kdg;

public class Actress {
    private String name;
    private int birthYear;

    public Actress(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public String getName() {
        return name;
    }
    public int getBirthYear() {
        return birthYear;
    }

    @Override
    public String toString() {
        return String.format("%s (%d)", name, birthYear);
    }

    //Make sure that actresses having the same name are considered as the same actress
    //Make sure that the natural sorting order of the actresses is first birthYear and then name
}
