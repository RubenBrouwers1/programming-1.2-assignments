import java.util.*;

public class Drivers {
    Map<String, Driver> driverByName = new HashMap<>();
    Map<Integer, Driver> driverByYear = new HashMap<>();

    //Constructor
    public Drivers(Map<String, Driver> driverByName, Map<Integer, Driver> driverByYear) {
        this.driverByName = driverByName;
        this.driverByYear = driverByYear;
    }

    public Drivers(String[][] champions, Integer[][] years) {
        for (int i = 0; i < champions.length; i++) {
            Driver driver = new Driver(champions[i][0],champions[i][1]);
            for(Integer year : years[i]) {
                driver.setYearsTheDriverWasChampion(year);
                driverByYear.put(year, driver);
            }
            driverByName.put(driver.getName(), driver);
        }
    }

    public Driver getDriverByName(String name){
        return (driverByName.get(name));
    }

    public Driver getDriverByYear(int year){
        return driverByYear.get(year);
    }

    Set<Driver> getDrivers(){
        Set<Driver> drivers= new TreeSet<>();
        int i=0;
        for (String name: driverByName.keySet()){
            drivers.add(driverByName.get(name));
            i++;
        }
        return drivers;
    }


}
