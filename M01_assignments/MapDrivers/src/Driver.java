import java.util.*;

public class Driver {
    private String name;
    private String nationality;

    TreeSet<Integer> yearsTheDriverWasChampion = new TreeSet<>();

//    interface Comparable<T> {
//        int compareTo(T t);
//    }

    //Constructor
    public Driver(String nationality, String name) {
        this.name = name;
        this.nationality = nationality;
    }

    //Getter
    public String getName() {
        return name;
    }
    public TreeSet<Integer> getYearsTheDriverWasChampion() {
        return yearsTheDriverWasChampion;
    }

    //Setters


    public void setYearsTheDriverWasChampion(Integer yearsTheDriverWasChampion) {
        this.yearsTheDriverWasChampion.add(yearsTheDriverWasChampion);
    }

    @Override
    public String toString() {
        return String.format("%s (%s) %d times champion %s", name, nationality, yearsTheDriverWasChampion.size(), yearsTheDriverWasChampion.toString());
    }
}
