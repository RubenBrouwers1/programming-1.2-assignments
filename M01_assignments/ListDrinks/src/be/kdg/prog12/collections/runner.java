package be.kdg.prog12.collections;

import be.kdg.prog12.collections.bar.Drink;
import be.kdg.prog12.collections.bar.Menu;

import java.util.Collections;

public class runner {
    public static void main(String[] args) {

        Menu menu = new Menu();

        menu.addDrinks(new Drink("LaChouffe", 3.50, true));
        menu.addDrinks(new Drink("Coca Cola", 2.00, false));
        menu.addDrinks(new Drink("Spa Sparkling", 2.00, false));
        menu.addDrinks(new Drink("Spa Still", 2.00, false));
        menu.addDrinks(new Drink("Coca Cola Light", 2.00, false));
        menu.addDrinks(new Drink("Coffee", 2.50, false));
        menu.addDrinks(new Drink("Tea", 2.50, false));
        menu.addDrinks(new Drink("Pils", 2.00, true));
        menu.addDrinks(new Drink("Duvel", 3.50, true));
        menu.addDrinks(new Drink("Orval", 4, true));

        System.out.printf("Our menu contains %d drinks with a total price of %.2f\n", menu.getSize(), menu.getTotalPrice());
        System.out.printf("Menu: %s\n", menu.getDrinks());
        System.out.printf("The menu with alcoholic drinks: %s", menu.getAlcoholicDrinks());
        System.out.printf("Our most expensive drink is: %s\n", menu.mostExpensiveDrink());
        menu.removeMoreExpensiveThen(3);
        System.out.printf("The menu where drinks are lower than €3: %s\n", menu.getDrinks());
    }
}
