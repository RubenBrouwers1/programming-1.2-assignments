package be.kdg.collections;

import java.util.*;

public class LottoDraw {
    public static void main(String[] args) {
        Random rd = new Random();

        Set<Integer> numbers = new TreeSet<Integer>();

        do {
            numbers.add(rd.nextInt(45)+1);
        } while (numbers.size() < 6);

        System.out.println("The lotto numbers are:");

        for (Integer number : numbers) {
            System.out.print(" " + number);
        }

    }
}
