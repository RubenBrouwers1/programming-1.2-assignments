// Your code here.
function loop(startValue, checker, action, whattodo) {
    for (let value = startValue; checker(value); value = action(value) ){
        whattodo(value)
    }
}

loop(3, n => n > 0, n => n - 1, console.log);
// → 3
// → 2
// → 1