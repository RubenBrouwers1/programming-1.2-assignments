  const box = {
    locked: true,
    unlock() { this.locked = false; },
    lock() { this.locked = true;  },
    _content: [],
    get content() {
      if (this.locked) throw new Error("Locked!");
      return this._content;
    }
  };
  
  function withBoxUnlocked(body) {
    
    if (!box.locked) {
        return body();
    }
    box.unlock();
    try {
        return body();
    }
    finally {
        box.lock();
    }
  }
  
  withBoxUnlocked( () => {
    box.content.push("gold piece", "crusty cum sock");
  });
  
  try {
    withBoxUnlocked( () => {
      throw new Error("Pirates on the horizon! Abort!");
    });
  } catch (e) {
    console.log("Error raised: " + e);
  }
  console.log(box.locked);

  try {
    console.log(box.content);
  } catch(e) {
    box.unlock();
    console.log(box.content);
  }
