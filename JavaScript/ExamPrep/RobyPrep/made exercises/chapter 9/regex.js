// Fill in the regular expressions

//car and cat
verify(/ca[rt]/,
    ["my car", "bad cats"],
    ["camper", "high art"]);

//pop and prop
verify(/pr?op/,
    ["pop culture", "mad props"],
    ["plop", "prrrop"]);

//ferret, ferry, and ferrari
//ferr[^u]\w*\b :pogchamp:
verify(/ferr(et|y|ari)/,
    ["ferret", "ferry", "ferrari"],
    ["ferrum", "transfer A"]);

//Any word ending in ious
verify(/\w*ious\b/,
    ["how delicious", "spacious room"],
    ["ruinous", "consciousness"]);

//A whitespace character followed by a period, comma, colon, or semicolon
verify(/\s[.,:;]/,
    ["bad punctuation ."],
    ["escape the period"]);

//A word longer than six letters
//[a-zA-Z]{6}
verify(/w{6}/,
    ["Siebentausenddreihundertzweiundzwanzig"],
    ["no", "three small words"]);

//A word without the letter e (or E)
verify(/[^\We]+\b/,
    ["red platypus", "wobbling nest"],
    ["earth bed", "learning ape", "BEET"]);


function verify(regexp, yes, no) {
// Ignore unfinished exercises
if (regexp.source == "...") return;
for (let str of yes) if (!regexp.test(str)) {
 console.log(`Failure to match '${str}'`);
}
for (let str of no) if (regexp.test(str)) {
 console.log(`Unexpected match for '${str}'`);
}
}

// Fill in this regular expression.
let number = /^[\+\-]?\d*(\.?\d+)?(\d+\.)?([eE][\+\-]?\d+)?$/;

//[\+\-]?\d*(\.?\d+)?(\d+\.)?([eE][\+\-]?\d+)?
//[+\-]?(\d+(\.\d*)?|\.\d+)([eE][+\-]?\d+)?

//^[+-]?.?\d{1}$

// Tests:
for (let str of ["1", "-1", "+5", "1.55", ".5", "5.",
                 "1.3e2", "1E-4", "1e+12"]) {
  if (!number.test(str)) {
    console.log(`Failed to match '${str}'`);
  }else{
    console.log("Successful match");
  }
}
for (let str of ["1a", "+-1", "1.2.3", "1+1", "1e4.5",
                 ".5.", "1f5", "."]) {
  if (number.test(str)) {
    console.log(`Incorrectly accepted '${str}'`);
  }
}