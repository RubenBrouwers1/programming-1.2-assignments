const MOUNTAINS = [
    {name: "Kilimanjaro", height: 5895, place: "Tanzania"},
    {name: "Everest", height: 8848, place: "Nepal"},
    {name: "Mount Fuji", height: 3776, place: "Japan"},
    {name: "Vaalserberg", height: 323, place: "Netherlands"},
    {name: "Denali", height: 6168, place: "United States"},
    {name: "Popocatepetl", height: 5465, place: "Mexico"},
    {name: "Mont Blanc", height: 4808, place: "Italy/France"}
  ];

const div = document.getElementById("mountains");
const table = document.createElement("table");

let tableHeaderRow = document.createElement("tr");
Object.keys(MOUNTAINS[0]).forEach(function(key) {
    let tableHead = document.createElement("th");
    tableHead.appendChild(document.createTextNode(key));
    tableHeaderRow.appendChild(tableHead);
});

table.appendChild(tableHeaderRow);

MOUNTAINS.forEach(value => {
    let tableDataRow = document.createElement("tr");
    Object.keys(MOUNTAINS[0]).forEach(function(key) {
        let tableData = document.createElement("td");
        tableData.appendChild(document.createTextNode(value[key]));
        if( typeof value[key] == "number"){
            tableData.style.textAlign = "right";
        }
        tableDataRow.appendChild(tableData);
    });
    table.appendChild(tableDataRow);
});


// Object.keys(MOUNTAINS[0]).forEach(function(key){
//     tableHead.appendChild(document.createTextNode());
//     tableHeaderRow.appendChild(tableHead);
// });


div.appendChild(table);

