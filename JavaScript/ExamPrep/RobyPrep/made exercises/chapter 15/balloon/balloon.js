const balloon = document.querySelector("p");
let textSize = 12;
console.log(balloon);

function changeTextSize(e){
    if(e.key == "ArrowUp"){
        textSize *= 1.1;
    }else{
        textSize /= 1.1;
    }
    if(textSize > 50){
        balloon.innerText = "💥";
        document.body.removeEventListener("keydown", changeTextSize);
    }
    balloon.style.fontSize = textSize + "px";
}

document.body.addEventListener("keydown", changeTextSize);

localStorage.setItem("myCat", ["Tom","Jerry"]);

const cats = JSON.stringify(localStorage.getItem("myCat"));

console.log(cats);
