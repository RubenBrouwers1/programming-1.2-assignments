let arrays = [[1, 2, 3], [4, 5], [6]];

function flattenArray(array){
    let singleArray = [];
    singleArray = array.reduce((previous, current) => previous.concat(current), []);
    console.log(singleArray);
}

flattenArray(arrays);