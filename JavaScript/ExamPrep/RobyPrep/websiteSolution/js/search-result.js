const searchParams = new URLSearchParams(window.location.search);
const nokMessage = document.getElementById('result_nok');
const tableBody = document.getElementById('table_body');

if (searchParams.has('query')) {
    nokMessage.style.visibility = 'hidden';
    fetchBooks(searchParams.get('query'));
} else {
    nokMessage.style.visibility = 'visible';
}

async function fetchBooks(query) {
    const response = await fetch(`https://www.googleapis.com/books/v1/volumes?q=${query}`);
    if (response.ok) {
        const result = await response.json();
        processItems(result.items);
    } else {
        nokMessage.style.visibility = 'visible';
    }
}

function processItems(items) {
    items.forEach(item => processVolumeInfo(item.volumeInfo));
}

function processVolumeInfo(volumeInfo) {
    const titleColumn = document.createElement('td');
    titleColumn.innerText = volumeInfo.title;
    const authorsColumn = document.createElement('td');
    if (volumeInfo.authors) {
        authorsColumn.innerText = volumeInfo.authors.reduce((acc, value) => acc + ', ' + value);
    } else {
        authorsColumn.innerText = 'UNKNOWN';
    }
    const row = document.createElement('tr');
    row.appendChild(titleColumn);
    row.appendChild(authorsColumn);
    tableBody.appendChild(row);
}
