// We WON'T be entering strict mode explicitly since we'll be using JS modules (C10).
// 'use strict';
// We'll also use IntelliJ's checks, and (once we get more experience) we'll be configuring
// a "linter" at the level of the project.

// 'Eloquent JavaScript''s notation is not supported by IntelliJ:

// (number) -> number
function calculateSquare(a) {
    return a * a; // Pressing CTRL-Q on the function's name won't reveal type information.
}

// IntelliJ supports JSDoc out of the box: https://jsdoc.app/
// Syntax is similar to JavaDoc syntax.

/**
 * @param {number} n Just a number.
 * @returns {number} Twice the given number.
 */
function timesTwo(n) {
    return n * 2;
}

// We can even use JSDoc to annotate variables, although often
// IDEs can derive type information from initializers.

/**
 * Adding type information to a const doesn't provide any benefits,
 * and, as a result, is discouraged. (the IDE can derive the type)
 * @type {string} lastName
 */
const lastName = 'Mercury';

/**
 * Adding type information makes sense here since 'name' is
 * not a 'const', and we want to prevent its type from changing.
 * @type {string} name
 */
let name = 'Freddy';
name = 7; // <-- Warning!

/**
 * Adding type information makes sense here as well since we're not
 * initializing the variable at declaration.
 * @type {number} age
 */
let age;
age = '20'; // <-- Warning!
