const NAME_KEY = 'NAME';
const MODE_KEY = 'MODE';

const nameField = document.getElementById('name');
const radioFields = document.querySelectorAll('input[type=radio]');

// Name
nameField.addEventListener('keyup', () => {
    localStorage.setItem(NAME_KEY, nameField.value);
});

const storedName = localStorage.getItem(NAME_KEY);
if (storedName) {
    nameField.value = storedName;
}

// Mode
radioFields.forEach(radioField => {
    radioField.addEventListener('click', () => {
        localStorage.setItem(MODE_KEY, radioField.value);
        setMode(radioField.value);
    });
});

function setMode(mode) {
    document.body.style.backgroundColor = mode === 'dark' ? 'black' : 'white';
    document.body.style.color = mode === 'dark' ? 'white' : 'black';
}

const storedMode = localStorage.getItem(MODE_KEY);
if (storedMode) {
    radioFields.forEach(radioField => {
        radioField.checked = radioField.value === storedMode;
    });
    setMode(storedMode);
}
