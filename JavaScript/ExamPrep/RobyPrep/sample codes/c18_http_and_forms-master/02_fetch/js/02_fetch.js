/* fetch('https://pokeapi.co/api/v2/pokemon') // The 'fetch' function returns a promise.
    .then(response => {
        console.log(response.status);
        console.log(response.headers.get('Content-Type'));
        return response.json(); // The 'json' method returns a Promise as well!
    })
    .then(data => processPokemon(data.results)); */
init()

async function init(){
  const response = await fetch('https://pokeapi.co/api/v2/pokemon');
  console.log(response.status);
  console.log(response.headers.get('Content-Type'));
  const data = await response.json();
  processPokemon(data.results);
}

function processPokemon(pokeArray) {
    const rowElements = pokeArray
        .filter(pokemon => pokemon.name.startsWith('c'))
        .map(pokemon => {
            const tr = document.createElement('tr');
            const td1 = document.createElement('td');
            td1.innerText = pokemon.name;
            const td2 = document.createElement('td');
            const a = document.createElement('a');
            td2.append(a);
            a.innerText = pokemon.url;
            a.setAttribute('href', pokemon.url);

            tr.append(td1, td2);

            return tr;
        }).reduce((acc, row) => {
            acc.push(row);
            return acc;
        }, []);

    const tableElement = document.getElementsByTagName('table')[0];
    tableElement.append(...rowElements);
}
