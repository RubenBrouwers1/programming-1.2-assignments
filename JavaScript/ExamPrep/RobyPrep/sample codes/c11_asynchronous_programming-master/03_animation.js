const clockElement = document.getElementById('clock');

function formatNumber(number) {
    return (number >= 10 ? '' : '0') + number.toString();
}

function updateClock() {
    const date = new Date();
    clockElement.innerText = `${formatNumber(date.getHours())}:${formatNumber(date.getMinutes())}:${formatNumber(date.getSeconds())}`;
}

updateClock();

const arrowElement = document.getElementById('arrow');
arrowElement.style.display = 'inline-block';

let seconds;

function animate() {
    const date = new Date();
    const millis = date.getMilliseconds();

    if (seconds !== date.getSeconds()) {
        seconds = date.getSeconds();
        updateClock();
    }

    arrowElement.style.transform = `rotate(${Math.round(millis * 360 / 1000 - 90)}deg)`;

    requestAnimationFrame(animate);
}

requestAnimationFrame(animate);
