const prices = [5.99, 6.99, 5.50, 12.00, 8.90];

const total = prices.reduce((theTotalSoFar, currentPrice) => {
    return theTotalSoFar + currentPrice;
});

console.log("Total: " + total);

const people = ["Lars", "Jan", "Ann", "Piet", "Tony", "Tom", "Wim", "Dieter", "Antoon", "Charmaine"];
const text = people.reduce((textSoFar, currentPerson) => {
    return textSoFar + "<li>" + currentPerson + "</li>";
}, "<ul>") + "</ul>";

console.log("People: " + text);
