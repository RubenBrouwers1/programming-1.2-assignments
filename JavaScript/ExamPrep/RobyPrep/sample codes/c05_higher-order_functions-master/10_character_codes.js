// https://unicode.org/emoji/charts/emoji-zwj-sequences.html

const zwj = '\u200D';

const codePoints = [parseInt('1F636', 16), zwj.codePointAt(0), parseInt('1F32B', 16), parseInt('FE0F', 16)];
const result = codePoints
    .map(p => String.fromCodePoint(p))
    .join('');
console.log(result);

const openMouth = '😮';
const wind = '💨';

console.log(openMouth + zwj + wind);
console.log('❤' + zwj + '🔥');
console.log('🧑' + '🏾' + zwj + '🔬');
