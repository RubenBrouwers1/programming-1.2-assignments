/*
 * This function expects an array and a function with ONE parameter.
 * It will call the function on each element of the array.
 */
function arrayHandler(someArray, someFunction) {
    for (let i = 0; i < someArray.length; i++) {
        someFunction(someArray[i]);
    }
}

// Classic function syntax:
arrayHandler(["a", "b", "c"], function (someString) {
    console.log(someString.toUpperCase());
});

// Lambda syntax:
arrayHandler([4, 5, 6], (someNumber) => {
    // "2" is automatically converted to 2 (see CH1)
    // This is an example, we should never use quotes here!
    console.log(someNumber * "2");
});
