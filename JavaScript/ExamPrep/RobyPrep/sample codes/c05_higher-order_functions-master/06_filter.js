const myNumbers = [456, 98, 731, 845, 566, 333];

const evenNumbers = myNumbers.filter(i => i % 2 === 0);

/*
// Alternative, using regular a named function.
function isEven(i) {
    // Must return a boolean:
    return i % 2 === 0;
}
const evenNumbers = myNumbers.filter(isEven);
*/

console.log("Even numbers:");
console.log(evenNumbers);

console.log("Numbers with even indices:");
// Filter takes a function with ONE, TWO, or THREE parameters!
console.log(myNumbers.filter((value, index) => {
   return index % 2 == 0;
}));
