function getLoggingFunction(type) {
    switch (type) {
        case "log":
            return console.log;
        case "info":
            return console.info;
        case "debug":
            return console.debug;
        case "error":
            return console.error;
        default:
            // See CH1 for '`' and '$' syntax.
            // See CH8 for information on Exceptions.
            throw new Error(`I don't support '${type}'!`);
    }
}

const theFunctionThatINeed = getLoggingFunction("debug");

theFunctionThatINeed("Functions, functions, functions!");
