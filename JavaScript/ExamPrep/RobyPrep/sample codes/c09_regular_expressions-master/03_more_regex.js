// Sample 1
let text = 'colour harbour favour';
text = text.replace(/ou/g, 'o'); // Instead of 'g', replaceAll would work as well.
console.log(text + '\n');

// Sample 2
const harborIndex = text.search(/harbor/);
console.log(`Harbor starts at position ${harborIndex}.\n`);

// Sample 3
const pattern = /or/g;
pattern.lastIndex = 11;
const match = pattern.exec(text);
console.log(`Found 'or' at position ${match.index}.\n`);

// Sample 4
const pattern2 = /or/g;
let match2 = pattern2.exec(text);
while (match2) {
    console.log(`Match found at position ${match2.index}.`);
    match2 = pattern2.exec(text);
}
console.log();

// Sample 5: Greed
const sourceCode = 'function f(param1, param2) { console.log("Hello"); }';
const regex = /\((.*)\)/g;
console.log('Searching for parameters and arguments:');
for (let match of sourceCode.matchAll(regex)) {
    console.log(`MATCH: ${match[1]}`);
}
console.log();

const regexLessGreedy = /\((.*?)\)/g;
console.log('Searching for parameters and arguments, lees greedy:');
for (let match of sourceCode.matchAll(regexLessGreedy)) {
    console.log(`MATCH: ${match[1]}`);
}

