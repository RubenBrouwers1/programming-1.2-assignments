const british = [
    '23rd April 2030',
    '15th March 2022',
    '1st June 2021',
];
// const american = ['January 6th, 2019'];

const britishRegex = /(\d{1,2})(st|nd|rd|th) (\S+) (\d{4})/;

for (let britishDateString of british) {
    const match = britishDateString.match(britishRegex);
    if (match) {
        console.log(`'${britishDateString}' is a valid British date.`);
        const american = britishDateString.replace(britishRegex, '$3 $1$2, $4');
        console.log(`American format: ${american}`);

        const dateObject = new Date(`${match[1]} ${match[3]} ${match[4]}`);
        console.log(`Date object: ${dateObject}`);
        console.log();
    }
}

const mayTheFourth = new Date(2021, 4, 4);
const mayTheFourthNoon = new Date(2021, 4, 4, 12, 0);

console.log("May the fourth: " + mayTheFourth);
console.log("May the fourth at noon: " + mayTheFourthNoon);
console.log("May the fourth, milliseconds since 1970-01-01: " + mayTheFourth.getTime());
