class Address {
    constructor(recipient, postalCode, city, country) {
        this._recipient = recipient;
        this._postalCode = postalCode;
        this._city = city;
        this._country = country;
    }

    get recipient() {
        return this._recipient;
    }

    toString() {
        return `${this._postalCode} ${this._city}\n`
            + `${this._country.toUpperCase()}\n`;
    }
}

export {Address};
