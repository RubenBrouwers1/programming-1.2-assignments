import {HomeAddress} from './HomeAddress.js';
import {POBox} from './POBox.js';

const home = new HomeAddress('Karel de Grote University of Applied Sciences and Arts',
    'Nationalestraat', 5, 2000, 'Antwerpen', 'Belgium');
const poBox = new POBox('KdG', 9001, 2000, 'Antwerpen', 'Belgium');

const outputElement = document.getElementById('output_area');

outputElement.textContent = home.toString();
outputElement.textContent += '\n';
outputElement.textContent += poBox.toString();
