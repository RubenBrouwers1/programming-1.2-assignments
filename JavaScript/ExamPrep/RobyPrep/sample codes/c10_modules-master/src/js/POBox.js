import {Address} from './Address.js';

class POBox extends Address {
    static PO_BOX_PREFIX = 'PO Box';

    constructor(recipient, poBoxNr, postalCode, city, country) {
        super(recipient, postalCode, city, country);
        this._poBoxNr = poBoxNr;
    }

    toString() {
        return `${this.recipient}\n`
            + `${POBox.PO_BOX_PREFIX} ${this._poBoxNr}\n`
            + super.toString();
    }
}

export {POBox};
