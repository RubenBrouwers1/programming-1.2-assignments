import {Address} from './Address.js';

class HomeAddress extends Address {
    constructor(recipient, street, houseNr, postalCode, city, country) {
        super(recipient, postalCode, city, country);
        this._street = street;
        this._houseNr = houseNr;
    }

    toString() {
        return `${this.recipient}\n`
            + `${this._street} ${this._houseNr}\n`
            + super.toString();
    }
}

export {HomeAddress};
