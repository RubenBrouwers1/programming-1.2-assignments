//
// CLOCK
//

const clockElement = document.getElementById('clock');

function formatNumber(number) {
    return (number >= 10 ? '' : '0') + number.toString();
}

function updateClock() {
    const date = new Date();
    clockElement.innerText = `${formatNumber(date.getHours())}:${formatNumber(date.getMinutes())}:${formatNumber(date.getSeconds())}`;
}

updateClock(); // show the clock
setInterval(updateClock, 1000); // now, also update the clock every second

//
// ANIMATION
//

const arrowElement = document.getElementById('arrow');
arrowElement.style.display = 'inline-block';

function animate() {
    const millis = new Date().getMilliseconds();
    arrowElement.style.transform = `rotate(${Math.round(millis * 360 / 1000)}deg)`;

    requestAnimationFrame(animate);
}
requestAnimationFrame(animate);
