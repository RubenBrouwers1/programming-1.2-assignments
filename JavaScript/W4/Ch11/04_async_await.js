// Sample 1
console.log('Start of sample 1');

async function f() {
    return 'Async function\'s result';
}

f().then(result => console.log('Sample 1: ' + result));
console.log('End of sample 1');

// Sample 2
console.log('Start of sample 2');
const promise1 = f();
promise1
    .then(result => console.log('Sample 2: ' + result))
    .catch(reason => console.log(reason));
console.log('End of sample 2');

// Sample 3
console.log('Start of sample 3');

async function g() {
    console.log('Waiting for f');
    const result = await f();
    console.log('Sample 3: ' + result);
    console.log('Done waiting for f');
}

g();
console.log('End of sample 3');

// Sample 4
console.log('Start of sample 4');
const h = async () => {
    console.log('Async lambda syntax');
};
h();
console.log('End of sample 4');
