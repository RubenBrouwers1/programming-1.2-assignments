// Looping a triangle
console.log("\n---Exercise: Looping a triangle---");

let char = '#';
const MAXLENGTH = 7;

while (char.length <= MAXLENGTH) {
    console.log(char);
    char += "#";
}

//FizzBuzz
console.log("\n---Exercise: FizzBuzz---")

for (let i = 0; i <= 100; i++) {
    if (i % 3 === 0 && i % 5 === 0) {
        console.log("FizzBuzz");
    } else if (i % 5 === 0) {
        console.log("Buzz");
    } else if (i % 3 === 0) {
        console.log("Fizz");
    } else {
        console.log(i);
    }
}


//ChessBoard    
console.log("\n---Exercise: ChessBoard---");

const COLUMNS = 8;
const ROWS = 8;
const CHAR = "#";
let board = "";

for (let i = 0; i < ROWS; i++) {
    for (let j = 0; j < COLUMNS; j++) {
        if ((i + j) % 2 === 0) {
            board += CHAR;
        } else {
            board += " ";
        }
    }
    board += "\n"
}

console.log(board)

//Minimum
console.log("\n--Exercise: Minimum---");

function min(c1, c2) {
    if (c1 > c2) {
        return c2;
    } else {
        return c1;
    }
}

console.log(min(0, 10))
console.log(min(0, -10))

//Recursion
console.log("\n---Exercise: Recursion---")

function isEven(x) {
    if (x % 1 === 0 && x > 0) {
        if (x % 2 !== 0) {
            return false;
        } else if (x % 2 === 0) {
            return true
        }
    } else {
        return "??"
    }
}

console.log(isEven(50));
// → true
console.log(isEven(75));
// → false
console.log(isEven(-1));
// → ??

//Bean Counting
console.log("\n---Exercise: Bean Counting---");

function countChars(char, st) {
    let ctr = 0;
    for (let i = 0; i < st.length; i++) {
        if (st[i] === char) {
            ctr++;
        }
    }
    return ctr;
}

console.log(countChars("B", "BBC"));
console.log(countChars("k", "kakkerlak"));

//The sum of a range
console.log("\n---Exercise: The sum of a range---");

function range(x1, x2) {
    let range = [];
    for (x1; x1 <= x2; x1++) range.push(x1);
    return range;
}

function sum(x1) {
    let sum = 0;
    for (let x2 of x1) {
        sum += x2;
    }
    return sum;
}

console.log(range(1, 10));
// → [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
// console.log(range(5, 2, -1));
// → [5, 4, 3, 2]
console.log(sum(range(1, 10)));
// → 55

// Reversing an array
console.log("\n---Exercise: Reversing an array---")

function reverseArray(array) {
    let output = [];
    for (let i = array.length - 1; i >= 0; i--) {
        output.push(array[i]);
    }
    return output;
}

function reverseArrayInPlace(array) {
    for (let i = 0; i < Math.floor(array.length / 2); i++) {
        let old = array[i];
        array[i] = array[array.length - 1 - i];
        array[array.length - 1 - i] = old;
    }
    return array;
}

console.log(reverseArray(["A", "B", "C"]));
// → ["C", "B", "A"];
let arrayValue = [1, 2, 3, 4, 5];
reverseArrayInPlace(arrayValue);
console.log(arrayValue);
// → [5, 4, 3, 2, 1]

//A list
console.log("\n---Exercise: a list---")

function arrayToList(array) {
    let list = null;
    for (let i = array.length - 1; i >= 0; i--) {
        list = {value: array[i], rest: list};
    }
    return list;
}

function listToArray(list) {
    let array = [];
    for (let node = list; node; node = node.rest) {
        array.push(node.value);
    }
    return array;
}

function prepend(value, list) {
    return {value, rest: list};
}

function nth(list, n) {
    if (!list) return undefined;
    else if (n === 0) return list.value;
    else return nth(list.rest, n - 1);
}

console.log(arrayToList([10, 20]));
// → {value: 10, rest: {value: 20, rest: null}}
console.log(listToArray(arrayToList([10, 20, 30])));
// → [10, 20, 30]
console.log(prepend(10, prepend(20, null)));
// → {value: 10, rest: {value: 20, rest: null}}
console.log(nth(arrayToList([10, 20, 30]), 1));
// → 20

//Deep comparison
console.log("\n---Exercise: Deep Comparison");

function deepEqual(a, b) {
    if (a === b) return true;
    if (a == null || typeof a != "object" ||
        b == null || typeof b != "object") return false;
    let keysA = Object.keys(a), keysB = Object.keys(b);
    if (keysA.length !== keysB.length) return false;
    for (let key of keysA) {
        if (!keysB.includes(key) || !deepEqual(a[key], b[key])) return false;
    }
    return true;
}

let obj = {here: {is: "an"}, object: 2};
console.log(deepEqual(obj, obj));
// → true
console.log(deepEqual(obj, {here: 1, object: 2}));
// → false
console.log(deepEqual(obj, {here: {is: "an"}, object: 2}));
// → true