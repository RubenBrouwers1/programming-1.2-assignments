package be.kdg.contact.view;

import be.kdg.contact.model.Message;
import be.kdg.contact.model.Messenger;
import javafx.scene.control.Alert;

public class ContactPresenter {
    private final Messenger model;
    private final ContactView view;

    public ContactPresenter(Messenger model, ContactView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
    }

    private void addEventHandlers() {
        this.view.getSubmitButton().setOnAction(event -> {
            final String lastName = view.getLastNameInput().getText();
            final String firstName = view.getFirstNameInput().getText();
            final String email = view.getEmailInput().getText();
            final String messageBody = view.getMessageBodyInput().getText();

            try {
                Message message = new Message(lastName, firstName, email, messageBody);
                model.send(message);

                final Alert sendAlert = new Alert(Alert.AlertType.INFORMATION);
                sendAlert.setHeaderText("Message sent");
                sendAlert.setContentText(String.format("To: %s %s", lastName, firstName));
                sendAlert.showAndWait();

                updateView();
            } catch (Exception exc) {
                final Alert sendAlert = new Alert(Alert.AlertType.ERROR);
                sendAlert.setHeaderText("Error sending message");
                sendAlert.setContentText(exc.toString());
                sendAlert.showAndWait();
            }
        });
    }

    private void updateView() {
        view.clearInput();
    }
}
