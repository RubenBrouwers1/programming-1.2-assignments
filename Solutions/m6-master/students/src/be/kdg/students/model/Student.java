package be.kdg.students.model;

public class Student {
    private final String lastName;
    private final String firstName;
    private final int number;
    private final String group;

    public Student(String lastName, String firstName, int number, String group) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.number = number;
        this.group = group;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getNumber() {
        return number;
    }

    public String getGroup() {
        return group;
    }
}
