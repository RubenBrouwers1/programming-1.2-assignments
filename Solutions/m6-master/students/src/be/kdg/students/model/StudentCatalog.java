package be.kdg.students.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class StudentCatalog {
    private final List<Student> students;
    private final InputStream inputStream;

    public StudentCatalog() {
        this.students = new ArrayList<>();
        this.inputStream = this.getClass().getClassLoader().getResourceAsStream("students.txt");
        this.readStudents();
    }

    public List<Student> getStudents() {
        return this.students;
    }

    private void readStudents() {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line = reader.readLine();
            Student student;
            while (line != null) {
                String[] tokens = line.split(";");
                student = new Student(tokens[0], tokens[1], Integer.parseInt(tokens[2]), tokens[3]);
                students.add(student);
                line = reader.readLine();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
