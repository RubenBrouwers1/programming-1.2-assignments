package be.kdg.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        Path dir = Paths.get("module6");
        Path fileInDir = dir.resolve("my_file.txt");

        if (!Files.exists(dir)) {
            Files.createDirectory(dir);
        }

        if (!Files.exists(fileInDir)) {
            Files.createFile(fileInDir);
        }

        Files.write(fileInDir, "Lars Willemsens".getBytes());
        // OR: Files.write(fileInDir, List.of("Lars Willemsens"));
        // (There are many ways to do this...)

        Map<String, Object> attributes = Files.readAttributes(fileInDir, "*");
        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
            System.out.printf("%-20s: %s%n", entry.getKey(), entry.getValue());
        }

        Files.copy(fileInDir, dir.resolve("my_files_copy.txt"));
    }
}
