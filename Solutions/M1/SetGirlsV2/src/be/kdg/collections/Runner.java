package be.kdg.collections;

import java.util.*;

/**
 * Author: Jan de Rijke
 */
public class Runner {
	public static void main(String[] args) {

		List<Girl> listGirls = List.of(new Girl("An", 20),new Girl("Bea", 20),
			new Girl("Bea", 25), new Girl("Diana", 25),
			new Girl("Zoë", 18), new Girl("Ekaterina", 18),
			new Girl("Bea", 20));
		TreeSet<Girl> setGirls = new TreeSet<>(listGirls);
		System.out.printf("Set of %d girls: %s\n",setGirls.size(), setGirls);
		System.out.println("Last girl: " + setGirls.last());


		System.out.println("Girl before Diana: " + setGirls.lower(new Girl("Diana", 25)));
		System.out.println("Girl that would be after Dido (21): " + setGirls.ceiling(new Girl("Dido", 21)));
	}
}
