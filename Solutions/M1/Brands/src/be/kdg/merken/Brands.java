package be.kdg.merken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 De klasse Merken bevat een List<String> met de naam merken als enig attribuut.
 De constructor heeft als parameter een object van de klasse Data. De gegevens hiervan worden naar de List merken overgbracht.
 Vergeet niet eerst de ArrayList te maken.
 Voorzie een methode sorteer om de merken alfabetisch te sorteren.
 Voorzie een methode keerOm om de volgorde van de merken om te keren.
 Voorzie een toString-methode, zie de gewenste afdruk bij TestMerken.

 */
public class Brands {
    private List<String> merken;

    public Brands() {
        merken = new ArrayList<>();
        Collections.addAll(merken, Data.merken);
        /*
        int aantal = Data.merken.length;
        for (int i = 0; i < aantal; i++) {
            merken.add(Data.merken[i]);
        }
        */
    }

    public void alphabetic() {
        Collections.sort(merken);
    }

    public void alphabeticDescending() {
        Collections.sort(merken);
        Collections.reverse(merken);
    }

    public String toString() {
        return merken.toString();
    }
}
