package be.kdg.frequentie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
De klasse frequnetie bevat een int constante MAX met de waarde 10, een List<Integer> met de naam lijst en een gewone tabel van int met de naam frequenties.
Voorzie een constructor die een nieuwe ArrayList maakt op basis van de List die als attribuut meegegeven wordt.
Voorzie een private methode bepaalFrequenties waarin je voor elk van de int's in de lijst bepaalt hoeveel keer ze voorkomen.
Voorzie een methode toonFrequenties om de frequenties te tonen, roep hierin de vorige methode op.
Voorzie ook een toString-methode om de gehele inhoud van de lijst met 10 waarden per regel te kunnen afdrukken.
 */
public class Frequentie {
    private static final int MAX = 10;

    private List<Integer> lijst;
    private int[] frequenties;

    public Frequentie(List<Integer> getallen) {
        lijst = new ArrayList<>();
        lijst.addAll(getallen);
    }

    private void bepaalFrequenties() {
        frequenties = new int[MAX];
        Arrays.fill(frequenties, 0);

        for (int i = 0; i < MAX; i++) {
            frequenties[i] = Collections.frequency(lijst, i);
        }
    }

    public String toonFrequenties() {
        bepaalFrequenties();

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < frequenties.length; i++) {
            result.append(String.format("%d --> %2d\n", i, frequenties[i]));
        }
        return result.toString();
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < lijst.size(); i++) {
            result.append(lijst.get(i)).append(" ");
            if ((i + 1) % 10 == 0) {
                result.append('\n');
            }
        }
        return result.toString();
    }
}
