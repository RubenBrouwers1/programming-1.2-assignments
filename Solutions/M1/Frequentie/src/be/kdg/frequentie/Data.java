package be.kdg.frequentie;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
De klasse Data bevat een constante MAX met de waarde 100.
Als tweede static attribuut is een Random object met de naam generator opgenomen.
Tenslotte is er het attribuut getallen van het tpe List<Integer>.
Voorzie het nodige in de constructor. Zorg ervoor dat er een ArrayList van int gemaakt wordt die met MAX willekeurige waarden in de range 0..9 gevuld wordt.
Voorzie een getter getGetallen die de List teruggeeft.
 */
public class Data {
    private static final int MAX = 100;
    private static final Random generator = new Random();

    private List<Integer> getallen;

    public Data() {
        getallen = new ArrayList<>(MAX);
        for (int i = 0; i < MAX; i++) {
            getallen.add(generator.nextInt(10));
        }
    }

    public List<Integer> getGetallen() {
        return getallen;
    }
}
