package be.kdg.actress;

public class Actress implements Comparable<Actress> {
    private final String name;
    private final int birthYear;

    public Actress(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        return name.equals(((Actress) object).name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public int compareTo(Actress other) {
        int compareYear = birthYear - other.birthYear;
        return  (compareYear != 0) ? compareYear :  name.compareTo(other.name);
    }

    @Override
    public String toString() {
        return String.format("%s (%d)",name, birthYear );
    }
}

