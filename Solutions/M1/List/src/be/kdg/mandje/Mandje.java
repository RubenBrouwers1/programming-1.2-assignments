package be.kdg.mandje;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * De klasse mandje bevat een verzameling van Artikel-objecten.
 * Aan deze verzameling moet je artikels kunnen toevoegen en ook
 * artikels kunnen uit verwijderen. Een artikel met dezelfde naam
 * kan slechts éénmaal toegevoegd worden.
 */
public class Mandje {
    private List<Artikel> mandje;

    public Mandje() {
        mandje = new ArrayList<>();
    }

    /*
        Deze methode geeft de inhoud van het mandje in de vorm van
        een List terug. De inhoud van het mandje zelf kan niet gewijzigd
        worden.
     */
    public List<Artikel> getMandje() {
        return Collections.unmodifiableList(mandje);
    }

    /*
       Deze methode voegt een artikel aan het mandje toe
       (maar alleen als er nog geen artikel met dezelfde naam
       in voorkomt).
    */
    public void voegToe(Artikel artikel) {
        if (!mandje.contains(artikel)) {
            mandje.add(artikel);
        }
    }

    /*
        Deze methode verwijdert een artikel uit het mandje.
        Indien het artikel verwijderd is geeft ze true terug,
        als het artikel niet gevonden werd geeft ze false terug.
    */
    public boolean verwijder(Artikel artikel) {
        return mandje.remove(artikel);
    }

    /*
       Deze methode sorteert de inhoud van het mandje
       alfabetisch volgens de naam van het artikel.
    */
    public void sorteerVolgensNaam() {
        Collections.sort(mandje);
    }

    /*
       Deze methode sorteert de inhoud van het mandje
       volgens de prijs, van hoog naar laag.
    */
    public void sorteerVolgensPrijs() {
        Collections.sort(mandje, new PrijsComparator());
    }

    // Inner class
    private class PrijsComparator implements Comparator<Artikel> {
        public int compare(Artikel artikel, Artikel anderArtikel) {
            double prijs = artikel.getPrijs();
            double anderePrijs = anderArtikel.getPrijs();
            return Double.compare(anderePrijs, prijs);
        }
    }

    /*
        Deze methode toont de inhoud van het mandje, netjes in
        kolommen onder mekaar, met de prijzen afgerond in centen
     */
    public void toon() {
        for (Artikel artikel : mandje) {
            System.out.format("%-16s €%4.2f\n", artikel.getNaam(), artikel.getPrijs());
        }
    }
}
