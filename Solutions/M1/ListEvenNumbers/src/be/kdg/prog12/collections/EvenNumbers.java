package be.kdg.prog12.collections;

import java.util.*;

/**
 * Author: Jan de Rijke
 */
public class EvenNumbers {
	public static void main(String[] args) {
		Random random = new Random();
		List<Integer> numberList = new ArrayList<>();

		//20 random numbers:
		for (int i = 0; i < 20; i++) {
			numberList.add(random.nextInt(50) + 1);
		}
		System.out.println(numberList);

		//Copy to array:
		Integer[] numberArray = numberList.toArray(new Integer[0]);
		for (Integer integer : numberArray) {
			System.out.print(integer + ", ");
		}
		System.out.println();

		//Remove odd numbers. Does not work on fixed size array!
		List<Integer> evenNumbers = Arrays.asList(numberArray);
		//This works: List<Integer> evenNumbers = new ArrayList(Arrays.asList(numberArray));
		for (Iterator<Integer> iterator = evenNumbers.iterator(); iterator.hasNext(); ) {
			Integer next = iterator.next();
			if (next % 2 == 1) {
				iterator.remove();
			}
		}

		System.out.println(evenNumbers);

	}
}
