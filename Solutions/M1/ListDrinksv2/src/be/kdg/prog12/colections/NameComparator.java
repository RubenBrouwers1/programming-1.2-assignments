package be.kdg.prog12.colections;

import java.util.Comparator;

/**
 * Author: Jan de Rijke
 */
public class NameComparator implements Comparator<Drink> {

	@Override
	public int compare(Drink o1, Drink o2) {
		return o1.getName().compareTo(o2.getName());
	}
}
