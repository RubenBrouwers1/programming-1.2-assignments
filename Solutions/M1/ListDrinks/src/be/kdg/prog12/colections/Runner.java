package be.kdg.prog12.colections;

/**
 * Author: Jan de Rijke
 */
public class Runner {
	public static void main(String[] args) {
		Menu menu = new Menu();
		menu.addDrink(new Drink("LaChouffe",3.5,true));
		menu.addDrink(new Drink("Coca Cola",2,false));
		menu.addDrink(new Drink("Spa Sparkling",2,false));
		menu.addDrink(new Drink("Spa Still",2,false));
		menu.addDrink(new Drink("Coca Cola Light",2,false));
		menu.addDrink(new Drink("Coffee",2.5,false));
		menu.addDrink(new Drink("Tea",2.5,false));
		menu.addDrink(new Drink("Pils",2,true));
		menu.addDrink(new Drink("Duvel",3.5,true));
		menu.addDrink(new Drink("Orval",4,true));

		System.out.printf("Our menu contains %d drinks, with a total cost of €%.2f\n",menu.getSize(),menu.sumPrices());
		System.out.println( menu);
		System.out.println("Our most expensive drink is: " + menu.mostExpensive());
		System.out.println("Our alcoholic drinks are: " + menu.getAlcoholicDrinks());
		menu.removeMoreExpensiveThen(3);
		System.out.println(String.format("Menu without drinks above €3: %s" , menu));
		Drink[] newArrivals = {new Drink("Bushmills 10yr",7,true),new Drink("SpringBank 5yr", 5,true)};
		menu.addDrinks(newArrivals);
		System.out.println("Extended menu: "+menu);

//		Our menu contains 10 drinks, with a total cost of €26,00
//		Menu: [drinks=[LaChouffe €3,50, Coca Cola €2,00, Spa Sparkling €2,00, Spa Still €2,00, Coca Cola Light €2,00, Coffee €2,50, Tea €2,50, Pils €2,00, Duvel €3,50, Orval €4,00]]
//		Our most expensive drink is: Orval €4,00
//		Our alcoholic drinks are: [LaChouffe €3,50, Pils €2,00, Duvel €3,50, Orval €4,00]
//		Menu without drinks above €3: Menu: [drinks=[Coca Cola €2,00, Spa Sparkling €2,00, Spa Still €2,00, Coca Cola Light €2,00, Coffee €2,50, Tea €2,50, Pils €2,00]]
//		Extended menu: Menu: [drinks=[Coca Cola €2,00, Spa Sparkling €2,00, Spa Still €2,00, Coca Cola Light €2,00, Coffee €2,50, Tea €2,50, Pils €2,00, Bushmills 10yr €7,00, SpringBank 5yr €5,00]]

	}
}
