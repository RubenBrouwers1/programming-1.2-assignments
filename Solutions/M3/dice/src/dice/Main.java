package dice;

import dice.model.Dice;
import dice.view.DiceView;
import dice.view.Presenter;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by vochtenh on 25/01/2016.
 */
public class Main extends Application{
    @Override
    public void start(Stage primaryStage) {
        Dice model = new Dice();
        DiceView view = new DiceView();
        Presenter presenter = new Presenter(model, view);
        Scene scene = new Scene(view);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
