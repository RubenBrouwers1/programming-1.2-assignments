package be.kdg.prog12.background.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public class BackgroundView extends BorderPane {
	private Button backgroundButton;

	public Button getBackgroundButton() {
		return backgroundButton;
	}


	public BackgroundView() {
		initialiseNodes();
		layoutNodes();
	}

	private void initialiseNodes() {
		backgroundButton = new Button("Repaint");
	}

	private void layoutNodes() {
		setBottom(backgroundButton);
		setAlignment(backgroundButton, Pos.BOTTOM_RIGHT);
		setPadding(new Insets(10, 10, 10, 10));
	}


}
