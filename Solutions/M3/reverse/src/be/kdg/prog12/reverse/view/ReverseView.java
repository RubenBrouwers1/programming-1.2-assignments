package be.kdg.prog12.reverse.view;

import javafx.geometry.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class ReverseView extends GridPane {
private TextField input;
private Button reverseButton;
	public ReverseView() {
		initialiseNodes();
		layoutNodes();
	}

	 TextField getInput() {
		return input;
	}

	 Button getReverseButton() {
		return reverseButton;
	}

	private void initialiseNodes() {
		reverseButton = new Button("reverse");
		input = new TextField();
	}

	private void layoutNodes() {
		add(input, 0, 0);
		add(reverseButton, 0, 1);
		setHalignment(reverseButton, HPos.RIGHT);
		setVgap(10);
		setPadding(new Insets(10,10,10,10));
		// add/set … methodes
		// Insets, padding, alignment, …
	}

	// package-private Getters
	// for controls used by Presenter

}
