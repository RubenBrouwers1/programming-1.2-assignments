package be.kdg.prog12.reverse.model;

public class Reverse {

	private String text;

	public Reverse(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void reverse() {
		text = new StringBuilder(text).reverse().toString();
	}

}
