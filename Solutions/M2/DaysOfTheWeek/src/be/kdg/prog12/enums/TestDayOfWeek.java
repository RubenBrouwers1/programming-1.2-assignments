package be.kdg.prog12.enums;

public class TestDayOfWeek {
    public static void main(String[] args) {
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            System.out.println(dayOfWeek);
        }
    }
}
