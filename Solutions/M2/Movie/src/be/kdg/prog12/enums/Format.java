package be.kdg.prog12.enums;

public enum Format {
    VHS("VHS"), DVD("DVD"), BLU_RAY("Blu-ray");

    // Omitting this attribute and instead writing a
    // switch statment in 'toString' would be fine as well.
    private final String name;

    Format(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
