package be.kdg.prog12.enums;

public enum Audio {
    PCM("PCM"), DOLBY("Dolby"), DOLBY_HD("Dolby HD"), VHS("VHS"), DTS_HD("DTS HD");

    // Omitting this attribute and instead writing a
    // switch statment in 'toString' would be fine as well.
    private final String name;

    Audio(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
