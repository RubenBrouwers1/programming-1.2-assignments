package be.kdg.prog12.exceptions;

public class Fibonacci {
    private static final long MAX = 91;

    public static long finonacciNumber(int n) {
        if (n < 0) {
            throw new FibonacciException("Negative values are not allowed!");
        } else if (n > MAX) {
            throw new FibonacciException("The maximum value for type 'long' was exceeded!");
        }

        long first = 0;
        long second = 1;
        long number = 0;

        for (int i = 0; i < n; i++) {
            number = first + second;
            first = second;
            second = number;
        }

        return number;
    }
}
