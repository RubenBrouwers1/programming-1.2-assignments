package be.kdg.prog12.inner;

public class Movie {
    private final String title;
    private Integer year;
    private Actor leadingRole;
    private Actor supportingRole;

    public Movie(String title, Integer year, String leadingRoleName, String supportingRoleName) {
        this.title = title;
        this.year = year;
        this.leadingRole = leadingRoleName == null ? null : new Actor(leadingRoleName);
        this.supportingRole = supportingRoleName == null ? null : new Actor(supportingRoleName);
    }

    public Movie(String title, int year) {
        this(title, year, null, null);
    }

    public Movie(String title) {
        this(title, null, null, null);
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public Actor getLeadingRole() {
        return leadingRole;
    }

    public Actor getSupportingRole() {
        return supportingRole;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setLeadingRole(Actor leadingRole) {
        this.leadingRole = leadingRole;
    }

    public void setSupportingRole(Actor supportingRole) {
        this.supportingRole = supportingRole;
    }

    public class Actor {
        private String name;

        public Actor(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @Override
    public String toString() {
        return String.format("Title: %-40s; Year: %-7s; Cast: %s, %s",
                title,
                year == null ? "unknown" : String.valueOf(year),
                leadingRole == null ? "unknown" : leadingRole.name,
                supportingRole == null ? "unknown" : supportingRole.name);
    }
}
