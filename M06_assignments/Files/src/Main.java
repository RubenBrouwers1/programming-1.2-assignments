import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Path dir = Paths.get("module6");
        if(!Files.exists(dir)){
            try{
                Files.createDirectory(dir);
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
            System.out.println("I've created a dir.");
        }
        else{
            System.out.println("The dir already exists.");
        }

        Path path = dir.resolve("my_file.txt");
        //Path path = Paths.get("module6/my_file.txt");
        if (!Files.exists(path)) {
            try {
                Path file = Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (Files.exists(path)) {
            List<String> someText = List.of("Ihsan", "Hepsen");
            try{
                Files.write(path, someText);
            } catch (IOException e){
                System.out.println("Printing failed");
            }
        } else {
            System.out.println("File does not exist!");
        }

        if(Files.exists(path)){
            List<String> someText = List.of("Ruben", "Brouwers");
            try{
                Files.write(path, someText);
                System.out.println("Wrote to file.");
            }catch(IOException e){
                System.out.println("Writing failed!");
            }
        }

        try{
            Map<String, Object> metaData = Files.readAttributes(path, "*"/*BasicFileAttributes.class*/);
            for(Map.Entry<String, Object> entry : metaData.entrySet()){
                System.out.printf("%-20s: %s \n", entry.getKey(), entry.getValue());
            }
        }catch(IOException e){
            System.out.println("Reading attributes failed.");
        }

        Path path2 = Paths.get("my_file_copy.txt");
        try{
            Path file = Files.createFile(path2);
            Files.copy(path, path2);
            System.out.println("Copied files");
        } catch (IOException e){
            System.out.println("Error with copying file.");
            e.printStackTrace();
        }

    }
}
