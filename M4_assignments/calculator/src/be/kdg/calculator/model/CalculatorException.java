package be.kdg.calculator.model;

public class CalculatorException extends RuntimeException {
    CalculatorException(String msg) {
        super(msg);
    }
}
