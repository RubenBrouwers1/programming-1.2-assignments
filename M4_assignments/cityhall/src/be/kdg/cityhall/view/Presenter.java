package be.kdg.cityhall.view;

public class Presenter {
    private final CityHallPane view;

    public Presenter(CityHallPane view) {
        this.view = view;

        this.addEventHandlers();
    }

    private void addEventHandlers() {
        this.view.getNormal().setOnAction(actionEvent -> {
            this.view.resetEffect();
            updateView();
        });
        this.view.getBlackAndWhite().setOnAction(actionEvent -> {
            this.view.applyBlackAndWhiteEffect();
            updateView();
        });
        this.view.getSepia().setOnAction(actionEvent -> {
            this.view.applySepiaEffect();
            updateView();
        });
    }

    private void updateView(){

    }
}
