package be.kdg.birds.view;

import be.kdg.birds.model.BirdsModel;
import javafx.stage.Window;

public class BirdsPresenter {
    private BirdsModel model;
    private BirdsView view;
    public BirdsPresenter(BirdsModel model, BirdsView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }
    private void addEventHandlers() {
// Add event handlers (inner classes or
// lambdas) to view controls.
// In the event handlers: call model methods
// and updateView().
    }
    private void updateView() {
// fills the view with model data
    }
    public void addWindowEventHandlers () {
        Window window = view.getScene().getWindow();
// Add event handlers to window
    }
}
