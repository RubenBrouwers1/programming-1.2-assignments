package be.kdg.birds;

import be.kdg.birds.model.BirdsModel;
import be.kdg.birds.view.BirdsPresenter;
import be.kdg.birds.view.BirdsView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        BirdsModel model = new BirdsModel();
        BirdsView view = new BirdsView();
        BirdsPresenter presenter = new BirdsPresenter(model, view);
        primaryStage.setScene(new Scene(view));
        presenter.addWindowEventHandlers();
        primaryStage.setTitle("Birds");
        primaryStage.setHeight(400);
        primaryStage.setWidth(600);
        primaryStage.getIcons().add(new Image("angrybird.png"));
        primaryStage.show();
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
}
