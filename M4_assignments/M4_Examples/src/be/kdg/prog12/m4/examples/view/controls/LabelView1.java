package be.kdg.prog12.m4.examples.view.controls;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class LabelView1 extends BorderPane {
    private Label label;

    public LabelView1() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        Image imageOk = new Image("/angrybird.png");
        this.label = new Label("Accept", new ImageView(imageOk));
    }

    private void layoutNodes() {
        this.setCenter(this.label);
        BorderPane.setMargin(this.label, new Insets(10));
    }
}
