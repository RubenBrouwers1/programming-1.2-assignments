package be.kdg.prog12.m4.examples;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class TestKeyEvent extends Application {
    @Override
    public void start(Stage stage) {
        BorderPane borderPane = new BorderPane();
        TextField textField = new TextField();

        textField.setOnKeyTyped(event -> {
            if ("aeiou".contains(event.getCharacter())) {
                System.out.println(event.getCharacter());
            }
        });

        /*
        textField.setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if ("aeiou".contains(event.getCharacter())) {
                    System.out.println(event.getCharacter());
                }
            }
        });
        */

        borderPane.setCenter(textField);
        BorderPane.setMargin(textField, new Insets(30.0));
        stage.setScene(new Scene(borderPane));
        stage.setTitle("Vowels");
        stage.show();
    }
}
