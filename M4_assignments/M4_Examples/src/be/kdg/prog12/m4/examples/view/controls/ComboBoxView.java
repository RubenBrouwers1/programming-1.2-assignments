package be.kdg.prog12.m4.examples.view.controls;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.BorderPane;

public class ComboBoxView extends BorderPane {
    private ComboBox<String> names;

    public ComboBoxView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.names = new ComboBox<>();
        ObservableList<String> values =
                FXCollections.observableArrayList("Jane", "Jack", "Jim");
        this.names.setItems(values);
        this.names.getSelectionModel().select(0);
    }

    private void layoutNodes() {
        this.setCenter(this.names);
        BorderPane.setMargin(this.names, new Insets(10));
    }
}
