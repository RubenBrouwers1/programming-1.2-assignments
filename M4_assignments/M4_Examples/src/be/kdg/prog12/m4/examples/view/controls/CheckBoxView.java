package be.kdg.prog12.m4.examples.view.controls;

import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.BorderPane;

public class CheckBoxView extends BorderPane {
    private CheckBox agree;

    public CheckBoxView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.agree = new CheckBox("I agree " +
                "to the terms & conditions.");
    }

    private void layoutNodes() {
        this.setCenter(this.agree);
        BorderPane.setMargin(this.agree, new Insets(15.0));
    }
}
