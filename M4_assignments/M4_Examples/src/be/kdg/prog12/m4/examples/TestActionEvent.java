package be.kdg.prog12.m4.examples;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class TestActionEvent extends Application {
    @Override
    public void start(Stage stage) {
        BorderPane borderPane = new BorderPane();
        ToggleButton toggleButton = new ToggleButton("On / Off");
        toggleButton.setOnAction(event ->
                System.out.println(toggleButton.isSelected() ? "On" : "Off")
        );

        /*
        toggleButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println(toggleButton.isSelected()
                        ? "On" : "Off");
            }
        });
        */

        borderPane.setCenter(toggleButton);
        BorderPane.setMargin(toggleButton, new Insets(30.0));
        stage.setScene(new Scene(borderPane));
        stage.show();
    }
}
