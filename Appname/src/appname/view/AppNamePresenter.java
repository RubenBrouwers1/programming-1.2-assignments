package appname.view;
import appname.model.AppNameModel;

public class AppNamePresenter {
    private AppNameModel model;
    private AppNameView view;

    public AppNamePresenter(
            AppNameModel model, AppNameView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {}
    private void updateView() {/* fills view*/}
}

